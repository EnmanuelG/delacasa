<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class M_general extends CI_Model {
    #variables 

    var $_params = '';
    var $_pdo;
    var $_conn;
    var $_db = 'default';

    #region contructor 

    public function set_db($_db) {
        $this->_db = $_db;
    }

    public function __construct() {
        parent::__construct();
        $this->_db = 'default';
    }

    private function _setConnection() {
        $this->benchmark->mark('conec_start');
        $CI = & get_instance();
        $parametros = ($this->_db != 'default') ? $CI->load->database($this->_db, TRUE) : NULL;

        $hostname = $parametros ? $parametros->hostname : $CI->db->hostname;
        $database = $parametros ? $parametros->database : $CI->db->database;
        $puerto = $parametros ? $parametros->port : $CI->db->port;
        $char_set = $parametros ? $parametros->char_set : $CI->db->char_set;
        $username = $parametros ? $parametros->username : $CI->db->username;
        $pass = $parametros ? $parametros->password : $CI->db->password;

        try {
            $this->_conn = new PDO('mysql:host=' . $hostname . ';dbname=' . $database . ';port=' . $puerto . ';charset=' . $char_set . ';', $username, $pass);
            $this->_conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->benchmark->mark('conec_end');
            log_message('info', 'Conexion a: "' . $this->_db . '" realizada correctamente, duración: ' . $this->benchmark->elapsed_time('conec_start', 'conec_end') . ' seg.');
        } catch (Exception $exc) {
            log_message('error', 'Error al crear la conexion: ' . $exc->getMessage());
            echo $exc->getTraceAsString();
        }
    }

    public function __destruct() {
        $this->_conn = NULL;
        $this->_pdo = NULL;
    }

    /* ---------------------------------------------------------------------------- */
    /*
     *   @name function:
     *   @params:
     *   @return:
     *   @description:,(is_string($item))? PDO::PARAM_STR : PDO::PARAM_INT
     */

    public function _setParams($data) {
        if (!empty($data) && is_array($data)) {
            $count = 1;
            foreach ($data as $item):
                $this->_pdo->bindValue($count, $item);
                $count++;
            endforeach;
        }
    }

    /* ---------------------------------------------------------------------------- */
    /*
     *   @name function:
     *   @params:
     *   @return:
     *   @description:
     */

    public function _preparedParams($data) {
        $this->_params = '';
        if (!empty($data) && is_array($data)) {
            $count = count($data);
            while ($count) {
                $this->_params .= '?,';
                $count --;
            }
        }
        $this->_params = substr($this->_params, 0, -1);
    }

//    public function beginTransaction() {
//        $this->_setConnection();
//        $this->_conn->beginTransaction();
//    }

//    public function commitear($procedure) {
//        try {
//            $this->_conn->commit();
//        } catch (Exception $exc) {
////            var_dump($exc);
//            $this->_conn->rollBack();
//            log_message('error', 'Error al commitear Procedimiento <' . $procedure . '> : ' . $exc->getMessage());
//            return $exc;
//        }
//    }

    /* ---------------------------------------------------------------------------- */
    /*
     *   @name function:
     *   @params:
     *   @return:
     *   @description:
     */

    public function execProcedure($procedure, $param) {
        $this->_setConnection();
        try {
            $this->benchmark->mark('proc_start');

            $this->_preparedParams($param);

            $this->_pdo = $this->_conn->prepare("CALL $procedure($this->_params);");

            $this->_setParams($param);

            $result = $this->_pdo->execute();

            $this->benchmark->mark('proc_end');

            log_message('info', 'Ejecución de: "' . $procedure . '", duración: ' . $this->benchmark->elapsed_time('proc_start', 'proc_end') . ' seg.');

            return $result;
        } catch (PDOException $exc) {
//            var_dump($exc);
            log_message('error', 'Error en Procedimiento <' . $procedure . '> : ' . $exc->getMessage());
            return $exc;
        }
    }
//
//    public function lastInsertId() {
//        return $this->_conn->lastInsertId();
//    }


    /* ---------------------------------------------------------------------------- */
    /*
     *   @name function:
     *   @params:
     *   @return:
     *   @description:
     */

    public function _result_all_array() {
        return $this->_pdo->fetchAll(PDO::FETCH_ASSOC);
    }

    /* ---------------------------------------------------------------------------- */
    /*
     *   @name function:
     *   @params:
     *   @return:
     *   @description:
     */

    public function _result_all_obj() {
        return $this->_pdo->fetchAll(PDO::FETCH_OBJ);
    }

    /* ---------------------------------------------------------------------------- */
    /*
     *   @name function:
     *   @params:
     *   @return:
     *   @description:
     */

    public function _result_obj() {
        return $this->_pdo->fetchObject();
    }

    /* ---------------------------------------------------------------------------- */
    /*
     *   @name function:
     *   @params:
     *   @return:
     *   @description:
     */

    public function _result_rowCount() {
        return $this->_pdo->rowCount();
    }

    /* ---------------------------------------------------------------------------- */
    /*
     *   @name function:
     *   @params:
     *   @return:
     *   @description:
     */

    public function _setBitacora($data) {
        $this->_params = '';
        $this->_setParams($params);
        $stm = $this->db->_execute("CALL ins_bitacora($this->_params);");
        $row = $stm->fetchAll();
        return ($row);
    }

    /* ---------------------------------------------------------------------------- */
    /*
     *   @name function:
     *   @params:
     *   @return:
     *   @description:
     */

    public function Upd_bitacora($params) {
        $procedure = 'ins_bitacora';
        return ($this->execProcedure($procedure, $params)) ? TRUE : FALSE;
    }

    /* ---------------------------------------------------------------------------- */
    /*
     *   @name function:
     *   @params:
     *   @return:
     *   @description:
     */

    public function get_GMT($params) {
        $procedure = 'get_gmt';
        return ($this->execProcedure($procedure, $params)) ? $this->_result_obj() : FALSE;
    }

    public function callProcedureResponse($procedimiento, $parametro, $db) {

        $this->set_db($db);
        $call = $this->execProcedure($procedimiento, $parametro);

        return ($call === TRUE) ? $this->_result_all_obj() : $call->getMessage();
    }

    /* ---------------------------------------------------------------------------- */
    /*
     *   @name function:
     *   @params:
     *   @return:
     *   @description:
     */

    public function get_countrycode($params) {
        $procedure = 'get_countrycode';
        return ($this->execProcedure($procedure, $params)) ? $this->_result_obj() : FALSE;
    }

    public function mysqlDateTimeToNormal($date) {
        $aux = explode(" ", $date);
        $aux2 = explode("-", $aux[0]);
        $temp = $aux2[2] . "/" . $aux2[1] . "/" . $aux2[0];
        $aux[0] = $temp;
        $result = implode(" ", $aux);
        return $result;
    }

    public function getUserDatabyToken($params) {
        $procedure = 'get_usuario_data';
        return ($this->execProcedure($procedure, $params)) ? $this->_result_obj() : FALSE;
    }

    public function limpiarString($texto) {
        $textoLimpio = preg_replace('([^A-Za-z])', '', $texto);
        return $textoLimpio;
    }

}
