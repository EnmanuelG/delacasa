<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$base_url = base_url();
$user = $this->session->userData;


switch ($user->nivel){
    case 1:
        $permisos = 'admin';
    break;
    case 2:
        $permisos = 'cajero';
    break;
    case 3:
        $permisos = 'cocina';
    break;
    case 4:
        $permisos = 'adminitracion';
    break;    
        
}     
        
?>
<!DOCTYPE html>
<html>
    <head>
        <title>DelaCasa</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="<?= base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?= base_url() ?>assets/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?= base_url() ?>assets/css/bootstrap-toggle.min.css" rel="stylesheet">
        <link rel="icon" type="image/png" href="<?php echo base_url(); ?>assets/imag/icons/favicon.ico"/>
        <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyClbud6185Id2nosGO3ko4c9xRoE9t8snk"></script>
        <script type="text/javascript">
            var base_url = '<?= base_url(); ?>';
            localStorage.base_url = base_url;
        </script>
        <style>
            body {
                overflow-x: hidden;
                overflow-y: scroll;
                background-image:url(<?php base_url(); ?>assets/imag/fondoPrincipal.jpg);
                background-repeat: no-repeat;
                background-size: cover;
            }
            #scrool{
                    overflow-x: hidden;
                    overflow-y: scroll;
                    max-height: 260px;
                    margin: 3px;
/*                    height: 260px*/
                }
        </style>
    </head>
    <body>
        <div class="container-fluid">
            <nav class="navbar navbar-inverse">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <span class="navbar-brand">DelaCasa</span>
                    </div>
                    <ul class="nav navbar-nav">
                        <?php if (in_array($permisos, array('admin','cajero'))) :?>
                        <li value="1"><a href="#">Hacer Pedido</a></li>
                        <?php endif; ?>
                        <?php if (in_array($permisos, array('admin','cajero','cocina'))) :?>
                        <li value="2"><a href="#">ListPedido</a></li>
                        <?php endif; ?>
                        <?php if (in_array($permisos, array('admin','adminitracion'))) :?>
                        <li value="3"><a href="#">Reporte</a></li>
                        <?php endif; ?>
                        <?php if (in_array($permisos, array('admin','adminitracion'))) :?>
                        <li value="4"><a href="#">productos</a></li>
                        <?php endif; ?>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="#"><span class="glyphicon glyphicon-user"></span><?php echo ' ' . $user->usuario ?></a></li>
                        <li><a href="<?php echo base_url(); ?>acceso/login/salir"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                    </ul>
                </div>
            </nav>
            <div class="row" style="margin: 2px">
                <!--            <div class="col-md-1 col-sm-1"></div>   -->
                <div class="col-md-12 col-sm-12">
                    <div class="row" id="principal">

                    </div>
                </div>
                <!--          <div class="col-md-1 col-sm-1"></div>      -->
            </div>      
        </div>    
    </body>
    <script src="<?= base_url() ?>assets/js/jquery-3.2.1.min.js"></script>
    <script src="<?= base_url() ?>assets/js/bootstrap.min.js"></script>
    <script src="<?= base_url() ?>assets/js/bootstrap-toggle.min.js"></script>
    <script src="<?= base_url() ?>assets/js/notify.js"></script>
    <script src="<?= base_url() ?>assets/js/principal.js"></script>
    <script src="<?= base_url() ?>assets/js/carrito.js"></script>
    <script src="<?= base_url() ?>assets/js/maps.js"></script>
    <script src="<?= base_url() ?>assets/js/Cliente_ficha.js"></script>
    
</html>
