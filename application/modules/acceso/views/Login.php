<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$this->session->unset_userdata('userData');
?><!DOCTYPE html>
<html lang="en">
    <head>
        <title>DelaCasa</title>
          <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->      
          <script src="<?php echo base_url();?>assets/js/login/vendor/jquery/jquery-3.2.1.min.js"></script>
    <!--===============================================================================================-->	
           <link rel="icon" type="image/png" href="<?php echo base_url();?>assets/imag/icons/favicon.ico"/>
    <!--===============================================================================================-->
    <!--	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">-->
            <script src="<?= base_url() ?>assets/js/bootstrap.min.js"></script>
    <!--===============================================================================================-->
            <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/login/util.css">
            <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/login/main.css">
         
        
        <script type="text/javascript">
           localStorage.setItem('url','');
           localStorage.url = '<?php echo base_url()?>'; 
        </script>
    </head>
    <body>
        	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<div class="login100-form-title" style="background-image: url(<?php echo base_url();?>assets/imag/bg-01.jpg);">
					<span class="login100-form-title-1">
						De La Casa
					</span>
				</div>
                            <form class="login100-form validate-form" id="loginform">
					<div class="wrap-input100 validate-input m-b-26" data-validate="Username is required">
						<span class="label-input100">Username</span>
						<input class="input100" type="text" name="username" placeholder="Enter username">
						<span class="focus-input100"></span>
					</div>
					<div class="wrap-input100 validate-input m-b-18" data-validate = "Password is required">
						<span class="label-input100">Password</span>
						<input class="input100" type="password" name="password" placeholder="Enter password">
						<span class="focus-input100"></span>
					</div>
					<div class="flex-sb-m w-full p-b-30">
						<div class="contact100-form-checkbox">
<!--							<input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
							<label class="label-checkbox100" for="ckb1">
								Remember me
							</label>-->
						</div>

						<div>
<!--							<a href="#" class="txt1">
								Forgot Password?
							</a>-->
						</div>
					</div>
					<div class="container-login100-form-btn">
                                            <button class="login100-form-btn">Login</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	
<!--===============================================================================================-->
        
<!--===============================================================================================-->
<!--	<script src="<?php echo base_url();?>assets/js/login/vendor/animsition/js/animsition.min.js"></script>-->
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>assets/js/login/vendor/bootstrap/js/popper.js"></script>
	<script src="<?php echo base_url();?>assets/js/login/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<!--	<script src="<?php echo base_url();?>assets/js/login/vendor/select2/select2.min.js"></script>-->
<!--===============================================================================================-->
<!--	<script src="<?php echo base_url();?>assets/js/login/vendor/daterangepicker/moment.min.js"></script>-->
<!--	<script src="<?php echo base_url();?>assets/js/login/vendor/daterangepicker/daterangepicker.js"></script>-->
<!--===============================================================================================-->
<!--	<script src="<?php echo base_url();?>assets/js/login/vendor/countdowntime/countdowntime.js"></script>-->
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>assets/js/login/main.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/notify.js"></script>

    </body>
</html>