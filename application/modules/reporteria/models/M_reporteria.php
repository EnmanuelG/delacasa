<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class M_reporteria extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    
    public function get_PedidosDelivery($data){
        $consulta = "SELECT
                    p.`CodePedido`,
                    p.fecha_alta,
                    p.TotalPago,
                    (SELECT e.`local` FROM `det_local` e WHERE e.`idLocal` = p.idLocal) AS 'Local',
                    (SELECT t.`tipoPedido` FROM `ped_pedido_tipo` t WHERE t.`idTipoPed` = p.`idTipoPed`)  AS 'tipoPedido',
                    (SELECT o.`Pago` FROM `ped_tipo_pago` o WHERE `idpago` = p.Idpago) AS 'pago',
                    (SELECT CONCAT(c.`nombre`,' ', c.`apellido`) FROM `cli_cliente` c WHERE c.`idCliente` = p.`idCliente`) AS 'nombre'
                    FROM `ped_pedido` p
                    WHERE p.`idTipoPed` = 1
                    AND p.`fecha_alta` BETWEEN '".$data['fecha1']." 00:00:00' AND '".$data['fecha2']." 23:59:59'
                    ORDER BY p.`fecha_alta` ASC";
        $resul = $this->db->query($consulta);
        return $resul->result();
    }
    
    public function get_PedidosLocal($data){
        $consulta = "SELECT
                    p.`CodePedido`,
                    p.fecha_alta,
                    p.TotalPago,
                    (SELECT e.`local` FROM `det_local` e WHERE e.`idLocal` = p.idLocal) AS 'Local',
                    (SELECT t.`tipoPedido` FROM `ped_pedido_tipo` t WHERE t.`idTipoPed` = p.`idTipoPed`)  AS 'tipoPedido',
                    (SELECT o.`Pago` FROM `ped_tipo_pago` o WHERE `idpago` = p.Idpago) AS 'pago',
                    (SELECT CONCAT(c.`nombre`,' ', c.`apellido`) FROM `cli_cliente` c WHERE c.`idCliente` = p.`idCliente`) AS 'nombre'
                    FROM `ped_pedido` p
                    WHERE p.`idTipoPed` = 2
                    AND p.`fecha_alta` BETWEEN '".$data['fecha1']." 00:00:00' AND '".$data['fecha2']." 23:59:59'
                    ORDER BY p.`fecha_alta` ASC";
        $resul = $this->db->query($consulta);
        return $resul->result();
    }
    
    public function get_Pedidosfull($data){
        $consulta = "SELECT
                    p.`CodePedido`,
                    p.fecha_alta,
                    p.TotalPago,
                    (SELECT e.`local` FROM `det_local` e WHERE e.`idLocal` = p.idLocal) AS 'Local',
                    (SELECT t.`tipoPedido` FROM `ped_pedido_tipo` t WHERE t.`idTipoPed` = p.`idTipoPed`)  AS 'tipoPedido',
                    (SELECT o.`Pago` FROM `ped_tipo_pago` o WHERE `idpago` = p.Idpago) AS 'pago',
                    (SELECT CONCAT(c.`nombre`,' ', c.`apellido`) FROM `cli_cliente` c WHERE c.`idCliente` = p.`idCliente`) AS 'nombre'
                    FROM `ped_pedido` p
                    WHERE p.`fecha_alta` BETWEEN '".$data['fecha1']." 00:00:00' AND '".$data['fecha2']." 23:59:59'
                    ORDER BY p.`fecha_alta` ASC";
        $resul = $this->db->query($consulta);
        return $resul->result();
    }
    
}
