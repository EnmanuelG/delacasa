<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Carrito extends RH_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('M_carrito');
    }

    public function iniCarrito() {
        $data['dataCarrito'] = $this->session->userdata('cart');
        $this->load->view('carritoV', $data);
    }

    public function addCarrito() {
        $TempCarrit = $this->session->userdata('cart');
        $idproducto = $this->input->post('idProducto');
        $data['idProducto'] = $idproducto;
        $data['qt'] = $this->input->post('qt');
        $data['descripcion'] = $this->input->post('descripcion');
        $data['dataProducto'] = Modules::run('producto/producto/get_producto', $data['idProducto']);
        $producto[$idproducto] = $this->carritoItems($data);
        if (!is_array($TempCarrit)) {
            $TempCarrit = array();
            array_push($TempCarrit, $producto[$idproducto]);
            $this->session->set_userdata('cart', $TempCarrit);
        } else {
            $valida = $this->validaProCarrit($producto[$idproducto]->idP, $producto[$idproducto]->qt, $TempCarrit);
            if ($valida['val'] == true) {
                $this->session->set_userdata('cart', $valida['carrito']);
            } else {
                array_push($TempCarrit, $producto[$idproducto]);
                $this->session->set_userdata('cart', $TempCarrit);
            }
        }
        $ViewCarrito['carrito'] = Modules::run('carrito/iniCarrito');
        echo json_encode($ViewCarrito);
    }

    private function carritoItems($parametro) {
        $tipo = $parametro['dataProducto']->idTipo;

        switch ($tipo) {
            case 1:
                $itemsCarrito = new stdClass();
                $itemsCarrito->idP = $parametro['idProducto'];
                $itemsCarrito->qt = $parametro['qt'];
                $itemsCarrito->descripcion = $parametro['descripcion'];
                $itemsCarrito->name = $parametro['dataProducto']->producto;
                $itemsCarrito->price = $parametro['dataProducto']->precio_delivery;
                $itemsCarrito->type = $parametro['dataProducto']->idTipo;
                $itemsCarrito->items = '';
                $itemsCarrito->total = (int) $parametro['qt'] * $parametro['dataProducto']->precio_delivery;
                break;
            case 2:
                $sabores = $this->input->post('Sabores');
                $itemsCarrito = new stdClass();
                $itemsCarrito->idP = $parametro['idProducto'];
                $itemsCarrito->qt = $parametro['qt'];
                $itemsCarrito->descripcion = $parametro['descripcion'];
                $itemsCarrito->name = $this->validaID($parametro['idProducto'], $parametro['qt'], $parametro['dataProducto']->producto);
                $itemsCarrito->price = $parametro['dataProducto']->precio_delivery > 0 ? $parametro['dataProducto']->precio_delivery : $this->calsaborPrecio($sabores);
                $itemsCarrito->type = $parametro['dataProducto']->idTipo;
                $itemsCarrito->items = $this->BuilItems($sabores);
                $itemsCarrito->total = (int) $parametro['qt'] * $itemsCarrito->price;
                break;
            case 4:
                $itemsCarrito = new stdClass();
                $itemsCarrito->idP = $parametro['dataProducto']->idproducto;
                $itemsCarrito->qt = 1;
                $itemsCarrito->name = $parametro['dataProducto']->producto;
                $itemsCarrito->price = $parametro['dataProducto']->precio_delivery;
                $itemsCarrito->type = $parametro['dataProducto']->idTipo;
                $itemsCarrito->items = '';
                $itemsCarrito->total = (int) $itemsCarrito->qt * $itemsCarrito->price;
                break;
        }

        return $itemsCarrito;
    }

    public function delItemCarrito() {
        $idProd = $this->input->post('idPoD');
        $product = $this->input->post('product');
        $Carrito = $this->session->userdata('cart');
        $tempCarrito = array();
        foreach ($Carrito as $cart) {
            if ($cart->name != $product) {
                $tempCarrito[] = $cart;
            }
        }
        $this->session->set_userdata('cart', $tempCarrito);
        $ViewCarrito['carrito'] = Modules::run('carrito/iniCarrito');
        echo json_encode($ViewCarrito);
    }

    private function validaProCarrit($idCarPr, $cant, $Carrito) {
        $resut['val'] = false;
        foreach ($Carrito as $car) {
            if ($car->idP == $idCarPr) {
                if ($car->type == 1) {
                    $car->qt += $cant;
                    $car->total = $car->qt * $car->price;
                    $resut['val'] = true;
                }
            }
        }
        $resut['carrito'] = $Carrito;
        return $resut;
    }

    private function calsaborPrecio($data) {
        $cant = count($data);
        switch ($cant) {
            case 1:
                $precio = 0;
                foreach ($data as $items) {
                    $producto = Modules::run('producto/producto/get_producto', $items);
                    $precio = ($producto->precio_delivery);
                }
                break;
            case 2:
                $precio = 0;
                foreach ($data as $items) {
                    $producto = Modules::run('producto/producto/get_producto', $items);
                    $precio += ($producto->precio_delivery * 0.5);
                }
                break;
            case 3:
                echo 'producto promo';
                break;
        }
        return $precio;
    }

    private function BuilItems($data) {
        $dataItems = array();
        foreach ($data as $a) {
            $dataItems[$a] = $this->oblje($a);
        }
        return $dataItems;
    }

    private function oblje($a) {
        $items = new stdClass;
        $items->id = $a;
        $items->cant = 1;
        return $items;
    }

    private function validaID($id, $qt, $producto) {
        $carrito = $this->session->userdata('cart');
        $cont = 0;
        if (is_array($carrito)) {
            foreach ($carrito as $cart) {
                if ($cart->idP == $id) {
                    $cont++;
                }
            }
        }
        $total = $cont + $qt;
        $result = $producto . '-' . $total;
        return $result;
    }

    public function limpiarCarrito() {
        $this->session->unset_userdata('cart');
        $this->session->set_userdata('tipoPedido', 0);
        $ViewCarrito['carrito'] = Modules::run('carrito/iniCarrito');
        echo json_encode($ViewCarrito);
    }

    public function tipoPedido() {
        $tipo = (int) $this->input->post('tipoPedido');
        switch ($tipo) {
            case 1:
                $data['dataProducto'] = Modules::run('producto/producto/getproTipo', 4);
                $idproducto = $data['dataProducto']->idproducto;
                $producto[$idproducto] = $this->carritoItems($data);
                $TempCarrit = $this->session->userdata('cart');
                if (!is_array($TempCarrit)) {
                    $TempCarrit = array();
                    array_push($TempCarrit, $producto[$idproducto]);
                    $this->session->set_userdata('cart', $TempCarrit);
                } else {
                    array_push($TempCarrit, $producto[$idproducto]);
                    $this->session->set_userdata('cart', $TempCarrit);
                }
                $this->session->set_userdata('tipoPedido', 1);
                $ViewCarrito['carrito'] = Modules::run('carrito/iniCarrito');
                echo json_encode($ViewCarrito);
                break;
            case 0:
                $this->delcostoDelivery();
                $this->session->set_userdata('tipoPedido', 0);
                $ViewCarrito['carrito'] = Modules::run('carrito/iniCarrito');
                echo json_encode($ViewCarrito);
                break;
        }
    }

    function delcostoDelivery() {
        $Carrito = $this->session->userdata('cart');
        if (is_array($Carrito)) {
            $tempCarrito = array();
            foreach ($Carrito as $cart) {
                if ($cart->type != 4) {
                    $tempCarrito[] = $cart;
                }
            }
            $this->session->set_userdata('cart', $tempCarrito);
        }
    }

}
