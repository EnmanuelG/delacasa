<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="row">
<!--    <div class="well-sm">
            <div id="myCarousel" class="carousel slide" data-ride="carousel" style="margin-top: 0px">
                <div class="carousel-inner">
                    <div class="item active">
                        <center> 
                        <img src="<?php echo base_url().'assets/imag/producto/PEPPERONI.jpg' ?>" alt="pizza" style="height: 54px; float: left">
                        <img src="<?php echo base_url().'assets/imag/producto/PAPAS.jpg' ?>" alt="papas" style="height: 54px; float: left">
                        <img src="<?php echo base_url().'assets/imag/producto/LOMITO.jpg' ?>" alt="lomito" style="height: 54px; float: left">
                        <img src="<?php echo base_url().'assets/imag/producto/PolloCatu.jpg' ?>" alt="lomito" style="height: 54px; float: left">
                        </center>
                  </div>

                  <div class="item">
                      <center><img src="<?php echo base_url().'assets/imag/DeLaCasaLogo.png' ?>" alt="papas" style="height: 54px"></center>
                    
                  </div>

                  <div class="item">
                    <center>
                    <img src="<?php echo base_url().'assets/imag/producto/polloCatu.jpg' ?>" alt="lomito" style="height: 54px; float: left">  
                    <img src="<?php echo base_url().'assets/imag/producto/combinacionLomito.jpg' ?>" alt="lomito" style="height: 54px; float: left">
                    <img src="<?php echo base_url().'assets/imag/producto/combinacion.jpg' ?>" alt="lomito" style="height: 54px; float: left">  
                    <img src="<?php echo base_url().'assets/imag/producto/lomito2.jpg' ?>" alt="lomito" style="height: 54px; float: left">  
                    </center>
                  </div>
                </div>

                 Left and right controls 
                <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                  <span class="glyphicon glyphicon-chevron-left"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next">
                  <span class="glyphicon glyphicon-chevron-right"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>
        </div>
    <hr/> -->
    <div class="col-md-12 text-center table-responsive" style="border: #6c757d">
        <div class="row">
            <div class="col-md-6"><label style="padding: 7px"><span class="glyphicon glyphicon-shopping-cart"></span> Carrito</label></div>
            <div class="col-md-6"><label class="checkbox "><input type="checkbox"  onclick="tipoPedido();" <?php echo $tP = $this->session->userdata('tipoPedido') == 1 ? 'checked' : '' ?> name="tipoPed" value="1"><span class="glyphicon glyphicon-send"></span> Delivery</label></div>
            </di>
            <?php if (isset($dataCarrito) && !empty($dataCarrito)) : ?>
                <table class="table table-striped">
                    <thead>
                    <th>Producto</th>
                    <th>Cant</th>
                    <th>Precio</th>
                    <th>Eliminar</th>
                    </thead>
                    <tbody>
                        <?php
                        $total = 0;
                        foreach ($dataCarrito as $cart) :
                            if($cart->type == 4):
                                $nameDelvery = $cart->name;
                                $qtDelivery  = $cart->qt;
                                $costDelivery = number_format($cart->total, 0, ',', '.');?>
                        <?php else :?>
                            <tr>
                                <td><?php echo $cart->name; ?></td>
                                <td><?php echo $cart->qt; ?></td>
                                <td><?php echo number_format($cart->total, 0, ',', '.'); ?></td>
                                <td><?php if ($cart->type == 4) : ?> <?php else : ?><a><span class="glyphicon glyphicon-remove-sign" onclick="deleCarrit('<?php echo $cart->idP; ?>', '<?php echo $cart->name; ?>')"></span></a></td><?php endif; ?>                 
                            <tr>
                        <?php endif;?>        
                                <?php $total += $cart->total;?>
                        <?php endforeach; ?>
                           <?php if(isset($nameDelvery)) :?>     
                            <tr>
                                <td><?php echo $nameDelvery; ?></td>
                                <td><?php echo $qtDelivery; ?></td>
                                <td><?php echo $costDelivery; ?></td>
                                <td></td>
                            </tr>
                          <?php endif;?>  
                    </tbody>
                </table>
            <p class="">Total: <?php $this->session->set_userdata('totalCompra', $total);   echo number_format($total, 0, ',', '.'); ?></p>
                <div class="btn-group btn-group-justified">
                    <a href="#" class="btn btn-warning" onclick="limpiarCarrito()"><span class="glyphicon glyphicon-trash"></span> Limpiar</a>
                    <a href="#" class="btn btn-primary" id="CrearPedido" onclick="ViewsCliente()"  ><span class="glyphicon glyphicon-check"></span> Crear Pedido</a>   
                </div>
            <?php else: ?>
                <p class="alert alert-warning">El carrito esta vacio.</p>
            <?php endif; ?>
        </div>
        
    </div>