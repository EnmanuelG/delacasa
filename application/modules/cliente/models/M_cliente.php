<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class M_cliente extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    
    public function InsCliente($parametro){
        $telefono1 = isset($parametro->telefon1) && $parametro->telefon1 != '' ? $parametro->telefon1 : null;
        $data = array('nombre' => $parametro->nombre, 'apellido' =>$parametro->apell, 
                      'Ci'=> $parametro->CI, 'ruc' => $parametro->ruc, 'sexo' => $parametro->genero,
                      'telefono'=> $parametro->telefon, 'telefono1'=> $telefono1, 'fecha_alta' => date("Y-m-d H:i:s"));
        
        $this->db->insert('cli_cliente', $data);
        return $this->db->insert_id();
    }
    
    public function InsDireccion($parametro, $idCliente){
        $data = array('direccion' => $parametro->Direccion, 'calle1' =>$parametro->calle1, 
                      'calle2'=> $parametro->calle2, 'ciudad' => $parametro->ciudad,
                      'lat' => $parametro->latitud, 'long' => $parametro->longitud,
                      'nro_casa' => $parametro->nroCasa, 'idCliente'=> $idCliente);
        
        $this->db->insert('det_cliente_direccion', $data);
        return $this->db->insert_id();
    }

    public function getClienteCant($data){
        $this->db->select('count(*) AS cant',FALSE);
        $this->db->from('cli_cliente');
        $this->db->where('Ci',$data->CI);
        $this->db->or_where('telefono',$data->telefon);
        $result = $this->db->get();
        return $result->row()->cant;        
    }
    
    public function getClienteID($id){
        $this->db->select('*');
        $this->db->from('cli_cliente');
        $this->db->where('idCliente',$id);
        $result = $this->db->get();
        return $result->row(); 
    }
    
    public function getDirToClienteID($id){
        $this->db->select('*');
        $this->db->from('det_cliente_direccion');
        $this->db->where('idCliente',$id);
        $result = $this->db->get();
        return $result->row(); 
    }
    
    public function getClienteToCi($CI){
        $this->db->select('*');
        $this->db->from('cli_cliente');
        $this->db->where('Ci',$CI);
        $result = $this->db->get();
        return $result->row();        
    }
    
    public function getClienteToCell($telefon){
        $this->db->select('*');
        $this->db->from('cli_cliente');
        $this->db->where('telefono',$telefon);
        $this->db->or_where('telefono1',$telefon);
        $result = $this->db->get();
        return $result->row();        
    }
    
    public function getClienteToID($id){
        $this->db->select('*');
        $this->db->from('cli_cliente');
        $this->db->where('Ci',$CI);
        $this->db->where('telefono',$tel);
        $result = $this->db->get();
        return $result->row()->idCliente;        
    }
    
    public function updateCliente($NewData,$idClie){
        $data = array(
        'nombre' => $NewData->nombre,'apellido' => $NewData->apell,
        'Ci' => $NewData->CI, 'ruc' => $NewData->ruc,  
        'sexo' => $NewData->genero, 'telefono' => $NewData->telefon,
        'telefono1' => $NewData->telefon1);
        
        $this->db->where('idCliente', $idClie);
        $this->db->update('cli_cliente', $data);
    }
    
    public function updateDir($NewData,$idDir){
       $data = array('direccion' => $NewData->Direccion, 'calle1' => $NewData->calle1, 
              'calle2'=> $NewData->calle2, 'ciudad' => $NewData->ciudad,
              'lat' => $NewData->latitud, 'long' => $NewData->longitud,
              'nro_casa' => $NewData->nroCasa);
        
        $this->db->where('idDireccion', $idDir);
        $this->db->update('det_cliente_direccion', $data);
    }

}
