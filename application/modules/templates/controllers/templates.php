<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Templates extends RH_Controller {
    public function __construct() {
        parent::__construct();
       $this->load->model('Template_model');
    }

    public function menu(){
        $data['menu'] = $this->Template_model->get_menu();
        $data['local'] = $this->Template_model->get_dtosLocal();
        $this->load->view('Menu', $data);
    }
    
    public function local(){
        $data = $this->Template_model->get_dtosLocal();
        return $data;
    }

}
