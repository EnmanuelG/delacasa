<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<div class="row">
    <div class="panel-group">
        <div class="panel panel-default">
            <div class="panel-body text text-center"><h4>Confirma Pedido</h4></div>
        </div>
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-8">
                        <div class="panel panel-default">
                            <div class="panel panel-default">
<!--                                <div class="panel-heading text text-center"><h4>Direccion Envio</h4></div>-->
                                <div class="panel-body">
                                    <strong class="text text-center"><p>Dirección del cliente.</p></strong>
                                    <p><strong>Nombre: </strong><?php echo $ficha->nombre.' '. $ficha->apell;?></p>
                                    <p><strong>Ciudad: </strong><?php echo $ficha->ciudad;?></p>
                                    <p><strong>Direccion: </strong><?php echo $ficha->calle1.' Casi '.$ficha->calle2;?></p> 
                                    <p><strong>nro de casa: </strong><?php echo $ficha->nroCasa;?></p>
                                    <div class="btn-group btn-group-justified">
<!--                                        <a class="btn btn-info"><span class="glyphicon glyphicon-edit">ir a Ficha</span></a>-->
                                        <span class="pull-right"><input type="button" value="ir a Ficha" class="btn btn-info"></span>
                                    </div>   
                                </div>
                            </div>
                        </div>
                    </div>
<!--                    <div class="col-md-4">
                        <div class="panel panel-default">
                            <div class="panel panel-default">
                                <div class="panel-heading text text-center"><h4>Info Factura</h4></div>
                                <div class="panel-body">Panel Content</div>
                                <strong>Dirección de Envio.</strong>
                                <p></p> ruc
                                <p></p> nombre o persoana a facturar
                            </div>  
                        </div>
                    </div>-->
                    <div class="col-md-4">
                        <div class="panel panel-default">
                            <div class="panel panel-default">
<!--                                <div class="panel-heading text text-center"><h4>Accion Pago</h4></div>-->
                                <div class="panel-body">
                                     <strong class="text text-center"><p>Pedido</p></strong>
                                      <form>
                                          <div class="row">
                                                <div class="form-group">
                                                    <label  class="col-sm-6 control-label">paga con</label>
                                                    <div class="col-sm-6">
                                                        <input class="form-control" name="Monto" id="Monto" type="text" >
                                                    </div> 
                                              </div>
                                              <div class="col-md-12">
                                                 <label class="radio-inline">
                                                     <input type="radio" name="pago" value="1" checked>Efectivo
                                                 </label>
                                                  <label class="radio-inline">
                                                      <input type="radio" name="pago" value="2" >Tarjeta
                                                 </label>
                                              </div>
                                          </div>
                                          <br/>
                                          <div class="col-md-12">
                                              <button type="button" class="btn btn-primary pull-right" onclick="newPed()"><span class="glyphicon glyphicon-floppy-saved"></span> Guardar</button>
                                          </div>
                                      </form>
                                    </div>  
                                </div>
                            </div>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>