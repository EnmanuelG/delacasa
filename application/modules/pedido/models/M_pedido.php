<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class M_pedido extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    
    public function getPaga(){
        $this->db->select('*');
        $this->db->from('ped_tipo_pago'); 
        $this->db->where('estado',1);
        $resu = $this->db->get();
        return $resu->result();
    }
    
    public function InsPedido($parametro){
        $data = array('CodePedido' => $parametro['Cod'], 
                      'montoPago' =>$parametro['pagoCont'], 
                      'totalPago'=> $parametro['montoTotal'], 'fecha_alta' => $parametro['fechaAlta'], 'idDireccion' => $parametro['idDireccion'],
                      'idCliente'=> $parametro['idCliente'], 'idLocal'=> $parametro['idLocal'],'idTipoPed'=> $parametro['idtipoPed'],
                      'Idpago'=> $parametro['idPago'], 'estadoPedido' => 1);
        
        $this->db->insert('ped_pedido', $data);
        return $this->db->insert_id();
    }
    
    public function InsCarrito($parametro){
        $data = array('fechaPedido' => $parametro['fechaAlta'], 
                      'data' => json_encode($parametro), 
                      'idPedido'=> $parametro['idPedido']);
        
        $this->db->insert('ped_carrito', $data);
        return $this->db->insert_id();
    }
    
    public function CodOp(){
        $this->db->select("CONCAT(DAY(NOW()),MONTH(NOW()),SUBSTR(CURDATE(),3, 2),'-',REPLACE(TIME(NOW()),':','')) as 'Op'", FALSE);
        $retur = $this->db->get();
        return $retur->row()->Op;
    }
    
    public function fechaAlta(){
        $this->db->select("now() as fechaAlta", FALSE);
        $retur = $this->db->get();
        return $retur->row()->fechaAlta;
    }
    
    public function tipoPtoNombre($id){
        $this->db->select("tipoPedido");
        $this->db->from("ped_pedido_tipo");
        $this->db->where('idTipoPed',$id);
        $retur = $this->db->get();
        return $retur->row()->tipoPedido;
    }
    
    public function getPedido($id){
        $this->db->select("*");
        $this->db->from("ped_pedido");
        $this->db->where('idPedido',$id);
        $retur = $this->db->get();
        return $retur->row();
    }
    
    public function NamePago($id){
        $this->db->select("pago");
        $this->db->from("ped_tipo_pago");
        $this->db->where('idpago',$id);
        $retur = $this->db->get();
        return $retur->row()->pago;    
    }
    
    public function lisPedido($parametro){
        $this->db->select("*");
        $this->db->from("ped_pedido");
        $this->db->where('idLocal',$parametro[0]);
        if($parametro[1] != null && $parametro[2] != null ){
            $this->db->where('DATE(fecha_alta) BETWEEN "'.$parametro[1].'" AND "'.$parametro[2].'"');
        } else {
            $this->db->where('DATE(fecha_alta) = CURDATE()');
        }
        $retur = $this->db->get();
        
        return $retur->result();
    }
    
    public function getEstado(){
        $this->db->select('*');
        $this->db->from('ped_estado');
        $this->db->where('activo',1);
        
        $Res = $this->db->get();
        return $Res->result();
    }
    
    public function getDatosCarrito($id){
        $this->db->select('*');
        $this->db->from('ped_carrito');
        $this->db->where('idPedido',$id);
        
        $Res = $this->db->get();
        return $Res->row()->data;
    }
    
    public function getOp($id){
        $this->db->select('*');
        $this->db->from('ped_pedido');
        $this->db->where('idPedido',$id);
        $Res = $this->db->get();
        return $Res->row()->CodePedido;
    }
    
    public function estados($data){
        $this->db->set('estadoPedido',$data['idEstado']);
        $this->db->where('idPedido',$data['idPedido']);
        $this->db->update('ped_pedido');
    }

    public function getEstarows($id){
        $this->db->select('*');
        $this->db->from('ped_estado');
        $this->db->where('idped_estado',$id);
        $Res = $this->db->get();
        return $Res->row()->estado_pedido;
    }
}
