<?php
defined('BASEPATH') OR exit('No direct script access allowed');
;?>
<div class="panel panel-success" style="margin-bottom: 0px; margin-top: 5px">
    <div class="panel-heading">
        <div class="text-center"><?php echo 'seleccione de 1-'.$cant.' sabores' ?></div>
    </div>
    <div class="panel-body">
        <div class="list-group-item" style="padding: 0; padding-left: 2px">  
            <div class="row" style="padding: 5px">
          <?php foreach ($sabores as $sabor) :?>  
                <div class="col-md-3">
                <div class="checkbox" id="ckesk1" style="padding: 0px; margin: 0px">
                    <label>
                    <input type="checkbox" value="<?php echo $sabor->pro_sabores;?>"form="formsimple" name="sabores">
                    <p title="<?php echo $sabor->saborDEsc;?>" class="text-warning"><?php echo $sabor->sabores;?></p>
                    </label>
                </div> 
                </div>
           <?php endforeach;?> 
        </div>   
        </div>
    </div>
</div>
