
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
    <div class="panel panel-primary" style="margin:1px">
        <div class="panel-heading text text-center">listado de productos</div>
        <div class="panel-body" id="lisProducto" style="padding: 0px;" >
            <div class="list-group" id="scrool">
                <?php foreach ($ListProductos as $produc):  
                    $stock = $produc->stock > 0 ? '' : 'list-group-item-danger';
                    ?>
                <a href="#" class="list-group-item <?php echo $stock;?>" onclick="Producto('<?php echo $produc->idproducto;?>', '<?php echo $produc->stock;?>')" >
                        <div class="row">
                            <div class="col-md-1">
                                <img class="" src="<?php echo base_url().'assets/imag/producto/'.$produc->imagen ?>" width="40" height="40" >
                            </div>
                            <div class="col-md-4">
                                <div class="text text-left text-uppercase h6"><strong><?php echo $produc->producto; ?></strong></div>
                            </div>
                            <div class="col-md-3">
                                <?php echo 'Precio: '. number_format($produc->precio_delivery,0 , ',', '.');?>
                            </div>
                            <div class="col-md-1 text text-right">
                                <?php echo 'Descripcion' ?>
                            </div>
                            <div class="col-md-3">
                                
                            </div>
                        </div>    
                    </a>
                <?php endforeach; ?>
            </div>
        </div>
    </div>   



