
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="panel panel-default">
    <div class="panel-heading text-center">Detalles  <?php echo $Producto->producto; ?></div>
    <div class="panel-body">
        <?php echo  Modules::run('producto/ViewsTipoPro', $Producto); ?>
    </div>
    <div class="panel-footer">
        <div class="row">
            <div class="col-sm-6 col-md-6">
                <button class="btn btn-warning   col-md-4 btn-sm col-sm-offset-8 clearfix span12" id="CancelProducto" onclick="get_Productos(-1)">
                    <span class="glyphicon glyphicon-remove-sign">Cancelar</span>
                </button>
            </div>
            <div class="col-sm-6 col-md-6">
                <button class="btn btn-primary btn-sm col-md-4 clearfix span12 pull-left" onclick="addcarrito()">
                    <span class="glyphicon glyphicon-plus" >Carrito</span>
                </button>
            </div>
        </div>
    </div>
</div>      