<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Producto extends RH_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('M_producto');
    }

    public function views($pro) {
        $this->load->view('productoV', $pro);
    }

    public function getListProducto() {
        $idcategoria = $this->input->post('idCategoria');
        $data['ListProductos'] = $this->M_producto->getProducto($idcategoria);
        $this->load->view('listProducto', $data);
    }

    public function viewsProducto() {
        $idProducto = $this->input->post('idproducto');

        $produc['Producto'] = $this->get_producto($idProducto);
        $view = 'producto/views';
        $data['producto'] = Modules::run($view, $produc);
        $this->load->view('productoV', $data);
    }
    
    
    public function ViewsTipoPro($data){
        
        switch ($data->idTipo){
            case 1:
                $Views = 'productSimple';
            break;
            case 2:
                $Views = 'productCompuesto';
            break;    
        }
        
        $this->load->view($Views, $data);
    }
    
    public function get_producto($idProducto){
        $producto = $this->M_producto->getProductoID($idProducto);
        return $producto;
    }
    
    public function productosabor($Producto){
           $idProducto = $Producto->idproducto;
           $data['sabores'] = $this->M_producto->getSabores($idProducto);
           $data['cant'] = $Producto->CantMax;
           $this->load->view('productoSabor',$data);
    }
    
    public function getproTipo($tipo){
        $producto = $this->M_producto->getCostoDelivery($tipo);
        return $producto;
    }
    
    public function admin(){
        $this->load->model('Templates/Template_model');
        $data['Menu'] = $this->Template_model->get_menu();
        $data['Producto'] = $this->M_producto->getProductoID('-1');
        $this->load->view('productoAdmint',$data);
    }
    
    public function getProdCate(){
         $idcategoria = $this->input->post('idCategoria');
         if($idcategoria != '-2'){
             $data['producto'] = $this->M_producto->getProductoAdmin($idcategoria);
         } else {
              $data['producto'] = $this->M_producto->getProductoSabores($idcategoria);
         }
         $data['html'] = Modules::run('producto/vieListProduc',$data);
        echo json_encode($data);
    }

    public function vieListProduc($data = '-1'){
        $this->load->view('productCrud', $data);
    }
    
    public function updatePro(){
      $data['cod'] =  $this->input->post('cod');
      $data['Prod'] = $this->input->post('Prod');
      $data['abr'] = $this->input->post('abr');
      $data['prec'] = $this->input->post('prec');
      $data['stock'] = $this->input->post('stock'); 
      $data['est'] = $this->input->post('est'); 
      $data['descrip'] = $this->input->post('descrip');
      $data['idProd'] = $this->input->post('idProd');
      $this->M_producto->updProd($data);
      $succes['categoriaID'] = $this->M_producto->CattoIdPro($data['idProd']);
      $succes['msn'] = 'Producto Actualizado';
      
      echo json_encode($succes);
    }
    
    public function deletProducto(){
        $data['idProd'] = $this->input->post('idProd');
        $succes['categoriaID'] = $this->M_producto->CattoIdPro($data['idProd']);
        $succes['msn'] = 'Producto Eliminado';
        $this->M_producto->deletPro($data['idProd']);
        
        echo json_encode($succes);
    }
    
    public function ViewsNewProduct(){
        $this->load->model('Templates/Template_model');
        $data['Categoria'] = $this->Template_model->get_menu();
        $data['tipo'] = $this->M_producto->getTipoPro();
        $data['Pizzas'] = $this->M_producto->getCompuestoP();
        $html['producto'] = Modules::run('producto/cargaView', $data);
        
        echo json_encode($html);
    }

    public function cargaView($data){
        $this->load->view('insProducto', $data);
    }
    
    public function insProducto(){  
        $detaP = $this->input->post('productos');
        $data = $this->datosP($detaP);
        
        $idProd = $this->M_producto->insProduct($data);
        if($data['tipo'] == '3'){
            $IdPizza = $data['pizza'];
            $this->insPizza($IdPizza, $idProd);
        }
        
        $html['msn'] = 'fue creado el producto: '.$data['product'].'';
        $html['Idcategoria'] = $data['categ'];
        echo json_encode($html);
    }
    
    private function datosP($detaP){
        $data = array();
        foreach ($detaP as $p){
            $data[$p['name']] = $p['value'];
        }
        return $data;
    }
    
    private function insPizza($IdPizza, $sabor){
        $data['idSabor'] = $sabor;
        $data['idPizza'] = $IdPizza;
        $this->M_producto->isnSabor($data);
    }
    
}

