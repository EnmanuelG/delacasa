/*
SQLyog Professional v12.09 (64 bit)
MySQL - 5.6.21 : Database - delacasa
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`delacasa` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `delacasa`;

/*Table structure for table `categoria_producto` */

DROP TABLE IF EXISTS `categoria_producto`;

CREATE TABLE `categoria_producto` (
  `idCategoria` int(11) NOT NULL AUTO_INCREMENT,
  `categoria` varchar(60) DEFAULT NULL,
  `subCategoria` varchar(45) DEFAULT NULL,
  `idLocal` int(11) NOT NULL,
  `orden` int(11) DEFAULT NULL,
  `estado` int(11) DEFAULT NULL,
  PRIMARY KEY (`idCategoria`,`idLocal`),
  KEY `fk_Ca_categoria_det_local_idx` (`idLocal`),
  CONSTRAINT `fk_Ca_categoria_det_local` FOREIGN KEY (`idLocal`) REFERENCES `det_local` (`idLocal`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

/*Data for the table `categoria_producto` */

insert  into `categoria_producto`(`idCategoria`,`categoria`,`subCategoria`,`idLocal`,`orden`,`estado`) values (1,'Pizzas',NULL,1,1,1),(3,'Bebidas',NULL,1,3,1),(5,'Lomitos/hamburguesa',NULL,1,2,1),(7,'Extra',NULL,1,4,1),(9,'Papas',NULL,1,5,1),(10,'sabores pizzas Grande',NULL,1,6,0),(11,'sabores pizzas personal',NULL,1,7,0),(12,'Potecito',NULL,1,8,1),(14,'Costo delivery',NULL,1,9,0);

/*Table structure for table `cli_cliente` */

DROP TABLE IF EXISTS `cli_cliente`;

CREATE TABLE `cli_cliente` (
  `idCliente` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(25) DEFAULT NULL,
  `apellido` varchar(45) DEFAULT NULL,
  `Ci` varchar(45) DEFAULT NULL,
  `ruc` varchar(45) DEFAULT NULL,
  `sexo` varchar(45) DEFAULT NULL,
  `ciudad` varchar(45) DEFAULT NULL,
  `telefono` varchar(11) DEFAULT NULL,
  `telefono1` varchar(11) DEFAULT NULL,
  PRIMARY KEY (`idCliente`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Data for the table `cli_cliente` */

/*Table structure for table `det_cliente_direccion` */

DROP TABLE IF EXISTS `det_cliente_direccion`;

CREATE TABLE `det_cliente_direccion` (
  `idDireccion` int(11) NOT NULL AUTO_INCREMENT,
  `direccion` varchar(45) DEFAULT NULL,
  `calle1` varchar(45) DEFAULT NULL,
  `calle2` varchar(45) DEFAULT NULL,
  `ciudad` varchar(45) DEFAULT NULL,
  `lat` varchar(45) DEFAULT NULL,
  `long` varchar(45) DEFAULT NULL,
  `nro_casa` int(11) DEFAULT NULL,
  `idCliente` int(11) NOT NULL,
  PRIMARY KEY (`idDireccion`,`idCliente`),
  KEY `fk_det_Cliente_direccion_Cli_cliente1_idx` (`idCliente`),
  CONSTRAINT `fk_det_Cliente_direccion_Cli_cliente1` FOREIGN KEY (`idCliente`) REFERENCES `cli_cliente` (`idCliente`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

/*Data for the table `det_cliente_direccion` */

/*Table structure for table `det_local` */

DROP TABLE IF EXISTS `det_local`;

CREATE TABLE `det_local` (
  `idLocal` int(11) NOT NULL AUTO_INCREMENT,
  `local` varchar(45) DEFAULT NULL,
  `direccion` varchar(45) DEFAULT NULL,
  `calle1` varchar(45) DEFAULT NULL,
  `call2` varchar(45) DEFAULT NULL,
  `lant` varchar(45) DEFAULT NULL,
  `long` varchar(45) DEFAULT NULL,
  `estado` int(11) DEFAULT NULL,
  `horario_ini` time DEFAULT NULL,
  `horario_fin` time DEFAULT NULL,
  `imagen` text,
  PRIMARY KEY (`idLocal`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `det_local` */

insert  into `det_local`(`idLocal`,`local`,`direccion`,`calle1`,`call2`,`lant`,`long`,`estado`,`horario_ini`,`horario_fin`,`imagen`) values (1,'Central','16 de julio','16 de julio','santiago leguizamon',NULL,NULL,1,'19:00:00','23:59:59','DeLaCAsaLogo.png');

/*Table structure for table `ped_carrito` */

DROP TABLE IF EXISTS `ped_carrito`;

CREATE TABLE `ped_carrito` (
  `idcarrito` int(11) NOT NULL AUTO_INCREMENT,
  `fechaPedido` datetime DEFAULT NULL,
  `data` text,
  `idPedido` int(11) NOT NULL,
  PRIMARY KEY (`idcarrito`,`idPedido`),
  KEY `fk_ped_carrito_ped_pedido1_idx` (`idPedido`),
  CONSTRAINT `fk_ped_carrito_ped_pedido1` FOREIGN KEY (`idPedido`) REFERENCES `ped_pedido` (`idPedido`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `ped_carrito` */

/*Table structure for table `ped_estado` */

DROP TABLE IF EXISTS `ped_estado`;

CREATE TABLE `ped_estado` (
  `idped_estado` int(11) NOT NULL AUTO_INCREMENT,
  `estado_pedido` varchar(45) DEFAULT NULL,
  `activo` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idped_estado`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `ped_estado` */

insert  into `ped_estado`(`idped_estado`,`estado_pedido`,`activo`) values (1,'En cocina','1'),(2,'Facturado','1'),(3,'enviado','1'),(4,'entregado','1');

/*Table structure for table `ped_pedido` */

DROP TABLE IF EXISTS `ped_pedido`;

CREATE TABLE `ped_pedido` (
  `idPedido` int(11) NOT NULL AUTO_INCREMENT,
  `montoPago` int(11) DEFAULT NULL,
  `TotalPago` int(11) DEFAULT NULL,
  `fecha_alta` datetime DEFAULT NULL,
  `idDireccion` int(11) NOT NULL,
  `idCliente` int(11) NOT NULL,
  `idLocal` int(11) NOT NULL,
  `idTipoPed` int(11) NOT NULL,
  `Idpago` int(11) NOT NULL,
  `estadoPedido` int(11) NOT NULL,
  PRIMARY KEY (`idPedido`,`idDireccion`,`idCliente`,`idLocal`,`idTipoPed`,`Idpago`,`estadoPedido`),
  KEY `fk_ped_pedido_Cli_direccion1_idx` (`idDireccion`,`idCliente`),
  KEY `fk_ped_pedido_det_local1_idx` (`idLocal`),
  KEY `fk_ped_pedido_ped_pedido_tipo1_idx` (`idTipoPed`),
  KEY `fk_ped_pedido_ped_tipo_pago1_idx` (`Idpago`),
  KEY `fk_ped_pedido_ped_estado1_idx` (`estadoPedido`),
  CONSTRAINT `fk_ped_pedido_Cli_direccion1` FOREIGN KEY (`idDireccion`) REFERENCES `det_cliente_direccion` (`idDireccion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ped_pedido_det_local1` FOREIGN KEY (`idLocal`) REFERENCES `det_local` (`idLocal`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ped_pedido_ped_estado1` FOREIGN KEY (`estadoPedido`) REFERENCES `ped_estado` (`idped_estado`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ped_pedido_ped_pedido_tipo1` FOREIGN KEY (`idTipoPed`) REFERENCES `ped_pedido_tipo` (`idTipoPed`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ped_pedido_ped_tipo_pago1` FOREIGN KEY (`Idpago`) REFERENCES `ped_tipo_pago` (`idpago`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `ped_pedido` */

/*Table structure for table `ped_pedido_tipo` */

DROP TABLE IF EXISTS `ped_pedido_tipo`;

CREATE TABLE `ped_pedido_tipo` (
  `idTipoPed` int(11) NOT NULL AUTO_INCREMENT,
  `tipoPedido` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idTipoPed`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `ped_pedido_tipo` */

insert  into `ped_pedido_tipo`(`idTipoPed`,`tipoPedido`) values (1,'delivery'),(2,'local');

/*Table structure for table `ped_tipo_pago` */

DROP TABLE IF EXISTS `ped_tipo_pago`;

CREATE TABLE `ped_tipo_pago` (
  `idpago` int(11) NOT NULL AUTO_INCREMENT,
  `Pago` varchar(45) DEFAULT NULL,
  `estado` int(11) DEFAULT NULL,
  PRIMARY KEY (`idpago`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `ped_tipo_pago` */

insert  into `ped_tipo_pago`(`idpago`,`Pago`,`estado`) values (1,'efectivo',1),(2,'tarjeta',1);

/*Table structure for table `pro_producto` */

DROP TABLE IF EXISTS `pro_producto`;

CREATE TABLE `pro_producto` (
  `idproducto` int(11) NOT NULL AUTO_INCREMENT,
  `code_producto` varchar(45) DEFAULT NULL,
  `producto` varchar(255) DEFAULT NULL,
  `descripcion` text,
  `abre` varchar(45) DEFAULT NULL,
  `precio_delivery` decimal(10,0) DEFAULT NULL,
  `precio_carry` decimal(10,0) DEFAULT NULL,
  `activo` int(11) DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  `imagen` varchar(45) DEFAULT NULL,
  `idCategoria` int(11) NOT NULL,
  `idTipo` int(11) NOT NULL,
  `CantMax` int(11) DEFAULT NULL,
  PRIMARY KEY (`idproducto`,`idCategoria`,`idTipo`),
  KEY `fk_pro_producto_Ca_categoria1_idx` (`idCategoria`),
  KEY `fk_pro_producto_tipo_producto1_idx` (`idTipo`),
  CONSTRAINT `fk_pro_producto_Ca_categoria1` FOREIGN KEY (`idCategoria`) REFERENCES `categoria_producto` (`idCategoria`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pro_producto_tipo_producto1` FOREIGN KEY (`idTipo`) REFERENCES `tipo_producto` (`idtipo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8;

/*Data for the table `pro_producto` */

insert  into `pro_producto`(`idproducto`,`code_producto`,`producto`,`descripcion`,`abre`,`precio_delivery`,`precio_carry`,`activo`,`stock`,`imagen`,`idCategoria`,`idTipo`,`CantMax`) values (1,'pizzg','pizza Grande','pizza tamaño grande 8 pedazo con sabores a elegir','pizzaG','0','0',1,1,'inconicto.jpg',1,2,2),(2,'pizzM','pizza personal','pizza personal 4 pedazos sabores a elegir','pizzap','0','0',1,1,'inconicto.jpg',1,2,2),(3,'','Coca cola 2lit','Gaseosa Coca cola 2lit',NULL,'12000','12000',1,1,'inconicto.jpg',3,1,NULL),(4,NULL,'Muzzarella','Salsa de tomate, muzzarella, orégano, aceitunas.','Muzza','23000','23000',1,1,'inconicto.jpg',10,3,NULL),(5,NULL,'Napolitana','Salsa de tomate, muzzarella, rodajas de tomate, ajo, orégano, aceitunas.','Napol','28000','28000',1,1,'inconicto.jpg',10,3,NULL),(6,NULL,'Jamón','Salsa de tomate, muzzarella, jamón, orégano, aceitunas.','jamon','28000','28000',1,1,'inconicto.jpg',10,3,NULL),(7,NULL,'Choclo','Salsa de tomate, muzzarella, choclo, orégano, aceitunas.','Choclo','28000','28000',1,1,'inconicto.jpg',10,3,NULL),(8,NULL,'Anchoa','Salsa de tomate, muzzarella, anchoa, orégano, aceitunas.','Anchoa','35000','35000',1,1,'inconicto.jpg',10,3,NULL),(9,NULL,'Mexicana','Salsa de tomate, salsa picante, muzzarella, tomate en rodajas, panceta, orégano, aceitunas.				','Mexic','35000','35000',1,1,'inconicto.jpg',10,3,NULL),(10,NULL,'Americana','Salsa de tomate, muzzarella, jamón, huevo duro en rodajas, panceta, orégano, aceitunas.			','America','35000','35000',1,1,'inconicto.jpg',10,3,NULL),(11,NULL,'Pollo','Salsa de tomate, muzzarella, pollo al cuchillo, cebolla dorada cortada salteada,orégano, aceitunas.','Pollo','35000','35000',1,1,'inconicto.jpg',10,3,NULL),(12,NULL,'Lomito de carne','Salsa de tomate, muzzarella, lomito al cuchillo, cebolla dorada cortada	salteada,orégano, aceitunas.','Lomi de Carn','35000','35000',1,1,'inconicto.jpg',10,3,NULL),(13,NULL,'Fernandina','Salsa de tomate, muzzarella, huevo duro en rodajas, panceta, orégano,aceitunas.','Fernandi','35000','35000',1,1,'inconicto.jpg',10,3,NULL),(14,NULL,'Pepperoni','Salsa de tomate, muzzarella, pepperoni, morrón verde, orégano, aceitunas.','Pepero','35000','35000',1,1,'inconicto.jpg',10,3,NULL),(15,NULL,'Palmito','Salsa de tomate, muzzarella, palmito, orégano, aceitunas.','palmito','35000','35000',1,1,'inconicto.jpg',10,3,NULL),(16,NULL,'Katupyry con Pollo','Salsa de tomate, muzzarella, pollo al cuchillo, cebolla dorada cortada salteada,queso katupyry, orégano, aceitunas.','poll y catupy','40000','40000',1,1,'inconicto.jpg',10,3,NULL),(17,NULL,'Katupyry con Lomito','Salsa de tomate, muzzarella, lomito al cuchillo, cebolla dorada cortada	salteada, queso katupyry, orégano, aceitunas.','Lomi y catupy','40000','40000',1,1,'inconicto.jpg',10,3,NULL),(18,NULL,'Champiñón','Salsa de tomate, muzzarella, champiñón, orégano, aceitunas.','Champin','35000','35000',1,1,'inconicto.jpg',10,3,NULL),(19,NULL,'Jamón y Morrón','Salsa de tomate, muzzarella, jamón, morrón rojo, orégano, aceitunas.','jamon y morr','35000','35000',1,1,'inconicto.jpg',10,3,NULL),(20,NULL,'Roquefort','Salsa de tomate, muzzarella, roquefort, orégano, aceitunas.','roquefor','35000','35000',1,1,'inconicto.jpg',10,3,NULL),(21,NULL,'Primavera','Salsa de tomate, muzzarella, jamón, tomate en rodajas, morrón verde, rojo y amarillo, orégano, aceitunas.','primavera','35000','35000',1,1,'inconicto.jpg',10,3,NULL),(22,NULL,'Cuatro Quesos','Salsa de tomate, muzzarella, roquefort, parmesano, katupyry, orégano, aceitunas.','4 Quesos','40000','40000',1,1,'inconicto.jpg',10,3,NULL),(23,NULL,'Cebollas','Salsa de tomate, muzzarella, cebollas moradas, blancas, orégano, aceitunas.','Cebollas','30000','30000',1,1,'inconicto.jpg',10,3,NULL),(24,NULL,'Katupyry','Salsa de tomate, muzzarella, queso katupyry, orégano, aceitunas.','katupyry','30000','30000',1,1,'inconicto.jpg',10,3,NULL),(25,NULL,'Atún','Salsa de tomate, muzzarella, atún al natural, orégano, aceitunas.','atun','30000','30000',1,1,'inconicto.jpg',10,3,NULL),(26,NULL,'Doble Queso','Salsa de tomate, extra muzzarella, orégano, aceitunas.','Doble Queso','30000','30000',1,1,'inconicto.jpg',10,3,NULL),(27,NULL,'Tropical','Salsa de tomate, piñas al almíbar, jamón de pavo, orégano, aceitunas negras.','Tropical','30000','30000',1,1,'inconicto.jpg',10,3,NULL),(28,NULL,'Vegetariana','Salsa de tomate, muzzarella, morrón verde, rojo, cebolla morada, tomate, choclo, champiñón, orégano, aceituna negra.','Vegeta','40000','40000',1,1,'inconicto.jpg',10,3,NULL),(29,NULL,'De la Casa','Salsa de tomate, muzzarella, tomate, morrones, cebollas salteadas, choclo, palmito, pepperoni, huevo duro, queso katupyry, orégano y aceitunas.			','De Casa','50000','50000',1,1,'inconicto.jpg',10,3,NULL),(30,NULL,'Lomito Simple','Lomito, tomate, jamón, queso, un potecito de salsa de ajo.','Lomito Simple','15000','15000',1,1,'inconicto.jpg',5,1,NULL),(31,NULL,'Lomito Completo','Lomito, tomate, jamón, queso, huevo, panceta, un potecito de salsa de ajo.','Lomito Completo','17000','17000',1,1,'inconicto.jpg',5,1,NULL),(32,NULL,'Papas pequeñas','papas fritas','Lomito Completo','17000','17000',1,1,'inconicto.jpg',9,1,NULL),(33,NULL,'Papas grandes','papas fritas','Lomito Completo','17000','17000',1,1,'inconicto.jpg',9,1,NULL),(34,NULL,'Muzzarella','Salsa de tomate, muzzarella, orégano, aceitunas.','Muzza','12000','12000',1,1,'inconicto.jpg',11,3,NULL),(35,NULL,'Napolitana','Salsa de tomate, muzzarella, rodajas de tomate, ajo, orégano, aceitunas.','Napol','15000','15000',1,1,'inconicto.jpg',11,3,NULL),(36,NULL,'Jamón','Salsa de tomate, muzzarella, jamón, orégano, aceitunas.','jamon','15000','15000',1,1,'inconicto.jpg',11,3,NULL),(37,NULL,'Choclo','Salsa de tomate, muzzarella, choclo, orégano, aceitunas.','Choclo','15000','15000',1,1,'inconicto.jpg',11,3,NULL),(38,NULL,'Anchoa','Salsa de tomate, muzzarella, anchoa, orégano, aceitunas.','Anchoa','18000','18000',1,1,'inconicto.jpg',11,3,NULL),(39,NULL,'Mexicana','Salsa de tomate, salsa picante, muzzarella, tomate en rodajas, panceta, orégano, aceitunas.				','Mexic','18000','18000',1,1,'inconicto.jpg',11,3,NULL),(40,NULL,'Americana','Salsa de tomate, muzzarella, jamón, huevo duro en rodajas, panceta, orégano, aceitunas.			','America','18000','18000',1,1,'inconicto.jpg',11,3,NULL),(41,NULL,'Pollo','Salsa de tomate, muzzarella, pollo al cuchillo, cebolla dorada cortada salteada,orégano, aceitunas.','Pollo','18000','18000',1,1,'inconicto.jpg',11,3,NULL),(42,NULL,'Lomito de carne','Salsa de tomate, muzzarella, lomito al cuchillo, cebolla dorada cortada	salteada,orégano, aceitunas.','Lomi de Carn','18000','18000',1,1,'inconicto.jpg',11,3,NULL),(43,NULL,'Fernandina','Salsa de tomate, muzzarella, huevo duro en rodajas, panceta, orégano,aceitunas.','Fernandi','18000','18000',1,1,'inconicto.jpg',11,3,NULL),(44,NULL,'Pepperoni','Salsa de tomate, muzzarella, pepperoni, morrón verde, orégano, aceitunas.','Pepero','18000','18000',1,1,'inconicto.jpg',11,3,NULL),(45,NULL,'Palmito','Salsa de tomate, muzzarella, palmito, orégano, aceitunas.','palmito','18000','18000',1,1,'inconicto.jpg',11,3,NULL),(46,NULL,'Katupyry con Pollo','Salsa de tomate, muzzarella, pollo al cuchillo, cebolla dorada cortada salteada,queso katupyry, orégano, aceitunas.','poll y catupy','20000','20000',1,1,'inconicto.jpg',11,3,NULL),(47,NULL,'Katupyry con Lomito','Salsa de tomate, muzzarella, lomito al cuchillo, cebolla dorada cortada	salteada, queso katupyry, orégano, aceitunas.','Lomi y catupy','20000','20000',1,1,'inconicto.jpg',11,3,NULL),(48,NULL,'Champiñón','Salsa de tomate, muzzarella, champiñón, orégano, aceitunas.','Champin','18000','18000',1,1,'inconicto.jpg',11,3,NULL),(49,NULL,'Jamón y Morrón','Salsa de tomate, muzzarella, jamón, morrón rojo, orégano, aceitunas.','jamon y morr','18000','18000',1,1,'inconicto.jpg',11,3,NULL),(50,NULL,'Roquefort','Salsa de tomate, muzzarella, roquefort, orégano, aceitunas.','roquefor','18000','18000',1,1,'inconicto.jpg',11,3,NULL),(51,NULL,'Primavera','Salsa de tomate, muzzarella, jamón, tomate en rodajas, morrón verde, rojo y amarillo, orégano, aceitunas.','primavera','18000','18000',1,1,'inconicto.jpg',11,3,NULL),(52,NULL,'Cuatro Quesos','Salsa de tomate, muzzarella, roquefort, parmesano, katupyry, orégano, aceitunas.','4 Quesos','20000','20000',1,1,'inconicto.jpg',11,3,NULL),(53,NULL,'Cebollas','Salsa de tomate, muzzarella, cebollas moradas, blancas, orégano, aceitunas.','Cebollas','15000','15000',1,1,'inconicto.jpg',11,3,NULL),(54,NULL,'Katupyry','Salsa de tomate, muzzarella, queso katupyry, orégano, aceitunas.','katupyry','15000','15000',1,1,'inconicto.jpg',11,3,NULL),(55,NULL,'Atún','Salsa de tomate, muzzarella, atún al natural, orégano, aceitunas.','atun','15000','15000',1,1,'inconicto.jpg',11,3,NULL),(56,NULL,'Doble Queso','Salsa de tomate, extra muzzarella, orégano, aceitunas.','Doble Queso','15000','15000',1,1,'inconicto.jpg',11,3,NULL),(57,NULL,'Tropical','Salsa de tomate, piñas al almíbar, jamón de pavo, orégano, aceitunas negras.','Tropical','18000','18000',1,1,'inconicto.jpg',11,3,NULL),(58,NULL,'Vegetariana','Salsa de tomate, muzzarella, morrón verde, rojo, cebolla morada, tomate, choclo, champiñón, orégano, aceituna negra.','Vegeta','20000','20000',1,1,'inconicto.jpg',11,3,NULL),(59,NULL,'De la Casa','Salsa de tomate, muzzarella, tomate, morrones, cebollas salteadas, choclo, palmito, pepperoni, huevo duro, queso katupyry, orégano y aceitunas.			','De Casa','25000','25000',1,1,'inconicto.jpg',11,3,NULL),(65,NULL,'Agregado de muzzarella	','Agregado de muzzarella','toppin Muzz','8000','8000',1,1,'inconicto.jpg',7,1,NULL),(66,NULL,'Agregado de choclo','Agregado de choclo','toppin chocl','5000','5000',1,1,'inconicto.jpg',7,1,NULL),(67,NULL,'Agregado de Aceitunas','Agregado de Aceitunas','toppin Aceit','2000','2000',1,1,'inconicto.jpg',7,1,NULL),(68,NULL,'Agregado de jamón','Agregado de jamón','toppin jamon','5000','5000',1,1,'inconicto.jpg',7,1,NULL),(69,NULL,'Agregado katupyry','Agregado katupyry','toppin katupy','10000','10000',1,1,'inconicto.jpg',7,1,NULL),(70,NULL,'Agregado de panceta','Agregado de panceta','toppin panceta','5000','5000',1,1,'inconicto.jpg',7,1,NULL),(71,NULL,'Potecito de salsa de ajo','Potecito de salsa de ajo','poteci ajo','1000','1000',1,1,'inconicto.jpg',12,1,NULL),(72,NULL,'Potecito de mayonesa','Potecito de mayonesa','poteci may','1000','1000',1,1,'inconicto.jpg',12,1,NULL),(73,NULL,'Potecito de mostaza','Potecito de mostaza','poteci most','1000','1000',1,1,'inconicto.jpg',12,1,NULL),(74,NULL,'Potecito de ketchup','Potecito de ketchup','poteci ketc','1000','1000',1,1,'inconicto.jpg',12,1,NULL),(75,NULL,'Agua con gas 500 ml','Agua con gas 500 ml','Agua c/gas 500','3000','3000',1,1,'inconicto.jpg',3,1,NULL),(76,NULL,'Agua sin gas 500 ml','Agua sin gas 500 ml','Agua s/gas 500','3000','3000',1,1,'inconicto.jpg',3,1,NULL),(77,NULL,'Agua tónica 500 ml','Agua tónica 500 ml','Agua tonik 500','5000','5000',1,1,'inconicto.jpg',3,1,NULL),(78,NULL,'Caipiriña','Caipiriña','kipiriña','25000','25000',1,1,'inconicto.jpg',3,1,NULL),(79,NULL,'Coca Cola 1.5 litros','Coca Cola 1.5 litros','coca 1.5lts','10000','10000',1,1,'inconicto.jpg',3,1,NULL),(80,NULL,'Vino Reservado Santa Helena Cabernet Sauvignon','Vino Reservado Santa Helena Cabernet Sauvignon','Vino Res SanH','28000','28000',1,1,'inconicto.jpg',3,1,NULL),(81,NULL,'Vino Tinto Santa Helena Gran Vino','Vino Tinto Santa Helena Gran Vino','Vino Tint SanH','28000','28000',1,1,'inconicto.jpg',3,1,NULL),(82,NULL,'Vino Tinto suave Quinta do Morgado','Vino Tinto suave Quinta do Morgado','Vino Tint QiM','28000','28000',1,1,'inconicto.jpg',3,1,NULL),(86,NULL,'Costo Delivery','Costo delivery|','Cost Delivery','7000','7000',1,1,'inconicto.jpg',14,4,NULL);

/*Table structure for table `pro_sabores` */

DROP TABLE IF EXISTS `pro_sabores`;

CREATE TABLE `pro_sabores` (
  `idpro_sabores` int(11) NOT NULL AUTO_INCREMENT,
  `estado` int(11) DEFAULT NULL,
  `precio` int(11) DEFAULT NULL,
  `pro_sabores` int(11) DEFAULT NULL,
  `idproducto` int(11) DEFAULT NULL,
  PRIMARY KEY (`idpro_sabores`),
  KEY `fk_pro_sabores_pro_producto1_idx` (`idproducto`),
  KEY `pro_sabores` (`pro_sabores`),
  CONSTRAINT `fk_pro_sabores_pro_producto1` FOREIGN KEY (`idproducto`) REFERENCES `pro_producto` (`idproducto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `pro_sabores_ibfk_1` FOREIGN KEY (`pro_sabores`) REFERENCES `pro_producto` (`idproducto`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8;

/*Data for the table `pro_sabores` */

insert  into `pro_sabores`(`idpro_sabores`,`estado`,`precio`,`pro_sabores`,`idproducto`) values (1,1,0,4,1),(2,1,0,5,1),(3,1,0,6,1),(4,1,0,7,1),(5,1,0,8,1),(6,1,0,9,1),(7,1,0,10,1),(8,1,0,11,1),(9,1,0,12,1),(10,1,0,13,1),(11,1,0,14,1),(12,1,0,15,1),(13,1,0,16,1),(14,1,0,17,1),(15,1,0,18,1),(16,1,0,19,1),(17,1,0,20,1),(18,1,0,21,1),(19,1,0,22,1),(20,1,0,23,1),(21,1,0,24,1),(22,1,0,25,1),(23,1,0,26,1),(24,1,0,27,1),(25,1,0,28,1),(26,1,0,29,1),(32,1,0,34,2),(33,1,0,35,2),(34,1,0,36,2),(35,1,0,37,2),(36,1,0,38,2),(37,1,0,39,2),(38,1,0,40,2),(39,1,0,41,2),(40,1,0,42,2),(41,1,0,43,2),(42,1,0,44,2),(43,1,0,45,2),(44,1,0,46,2),(45,1,0,47,2),(46,1,0,48,2),(47,1,0,49,2),(48,1,0,50,2),(49,1,0,51,2),(50,1,0,52,2),(51,1,0,53,2),(52,1,0,54,2),(53,1,0,55,2),(54,1,0,56,2),(55,1,0,57,2),(56,1,0,58,2),(57,1,0,59,2);

/*Table structure for table `tipo_producto` */

DROP TABLE IF EXISTS `tipo_producto`;

CREATE TABLE `tipo_producto` (
  `idtipo` int(11) NOT NULL AUTO_INCREMENT,
  `product_tipo` varchar(45) DEFAULT NULL,
  `descripcion` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idtipo`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `tipo_producto` */

insert  into `tipo_producto`(`idtipo`,`product_tipo`,`descripcion`) values (1,'simple','producto sin compuestos'),(2,'compuestos','pizzas / helados'),(3,'sabores','sabores de pizzas y helados'),(4,'costo Delivery','Costo delivery');

/*Table structure for table `usuarios` */

DROP TABLE IF EXISTS `usuarios`;

CREATE TABLE `usuarios` (
  `idusuario` int(11) NOT NULL AUTO_INCREMENT,
  `idLocal` int(11) NOT NULL,
  `usuario` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `nivel` int(11) DEFAULT NULL,
  `estado` int(11) DEFAULT NULL,
  PRIMARY KEY (`idusuario`,`idLocal`),
  KEY `fk_usuario_det_local1_idx` (`idLocal`),
  CONSTRAINT `fk_usuario_det_local1` FOREIGN KEY (`idLocal`) REFERENCES `det_local` (`idLocal`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `usuarios` */

insert  into `usuarios`(`idusuario`,`idLocal`,`usuario`,`password`,`nivel`,`estado`) values (1,1,'manu','adcd7048512e64b48da55b027577886ee5a36350',1,1);

/* Function  structure for function  `FN_getSaboresName` */

/*!50003 DROP FUNCTION IF EXISTS `FN_getSaboresName` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` FUNCTION `FN_getSaboresName`(VidProducto int) RETURNS varchar(25) CHARSET latin1
BEGIN
        declare sabor varchar(25);
	set sabor = (select `producto` from `pro_producto` where `idproducto` = VidProducto);
	return sabor;
    END */$$
DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
